-- phpMyAdmin SQL Dump
-- version 4.8.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Sep 11, 2019 at 09:04 AM
-- Server version: 5.7.24
-- PHP Version: 7.2.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `blood_record`
--

-- --------------------------------------------------------

--
-- Table structure for table `blood_bank`
--

CREATE TABLE `blood_bank` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `category` varchar(50) NOT NULL,
  `location` varchar(50) NOT NULL,
  `contact` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `blood_bank`
--

INSERT INTO `blood_bank` (`id`, `name`, `category`, `location`, `contact`) VALUES
(1, 'Islami Bank Hospital Blood Bank', 'private', 'Dhaka, Bangladesh', '8317090, 8321495'),
(2, 'Red Crescent Blood Bank', 'private', 'Dhaka, Bangladesh', '9116563, 8121497'),
(3, 'Sir Salimullah College Blood Bank', 'private', 'Dhaka, Bangladesh', '7319123'),
(4, 'Shandhani, Dhaka Dental College Branch', 'private', 'Dhaka Dental College Branch; Dhaka, Bangladesh', '9011887'),
(5, 'Shandhani, Dhaka Medical College Branch', 'local', 'Dhaka Medical College Branch; Dhaka, Bangladesh', ' 9668690,861674');

-- --------------------------------------------------------

--
-- Table structure for table `donars`
--

CREATE TABLE `donars` (
  `id` int(11) NOT NULL,
  `name` varchar(111) DEFAULT NULL,
  `gender` varchar(111) DEFAULT NULL,
  `blood_group` varchar(111) DEFAULT NULL,
  `age` int(111) DEFAULT NULL,
  `weight` int(111) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `contact` int(111) DEFAULT NULL,
  `email` varchar(150) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `donars`
--

INSERT INTO `donars` (`id`, `name`, `gender`, `blood_group`, `age`, `weight`, `address`, `contact`, `email`) VALUES
(2, 'Arif Uddin', 'male', 'A2 Negative(-)', 33, 57, 'Agrabad,Ctg.', 1855123476, 'arifuddin@gmail.com'),
(3, 'Abdullah Al Mamun', 'male', 'A2 Negative(-)', 25, 55, 'New market,Ctg.', 1955633488, 'mamunctg@gmail.com'),
(4, 'Nishat Rahman', 'male', 'A1B Negativ(-)', 28, 60, 'Gec,Ctg.', 1633123414, 'nishatpciu@gmail.com'),
(5, 'Sukkur Ali', 'male', 'A2B Positive(+)', 21, 65, 'South Kulshi,Ctg.', 1867812345, 'ctgsukkur@gmail.com'),
(6, 'Mazharul Islam', 'male', 'A Positive(+)', 23, 55, 'Bohaddarhat,Ctg', 1866443322, 'mazharulislam10000@gmail.com');

-- --------------------------------------------------------

--
-- Table structure for table `message`
--

CREATE TABLE `message` (
  `id` int(11) NOT NULL,
  `email` varchar(111) NOT NULL,
  `name` varchar(100) NOT NULL,
  `contact` varchar(100) NOT NULL,
  `message` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `message`
--

INSERT INTO `message` (`id`, `email`, `name`, `contact`, `message`) VALUES
(1, 'mazharulislam10000@gmail.com', 'Mazharul Islam', '01868135578', 'Hi I am Mazhar I need One bag Blood '),
(2, 'sdf', 'sdfs', 'sdf', 'sdfsadfs'),
(3, 'mazharulislam10000@gmail.com', 'Mazharul Islam', '01868135578', 'Hi I am MAZHARUL i need one bag blood '),
(4, 'mazharulislam10000@gmail.com', 'sdfsdfs', 'sdfsadfsdf', 'fsdfsdfsdf'),
(5, 'mazharulislam10000@gmail.com', 'Mazharul Islam', '01868135578', 'Hi I am mazed'),
(6, 'ctgsukkur@gmail.com', '', '', 'sadfsadfsadf');

-- --------------------------------------------------------

--
-- Table structure for table `request_blood`
--

CREATE TABLE `request_blood` (
  `id` int(11) NOT NULL,
  `name` varchar(111) NOT NULL,
  `contact` varchar(111) NOT NULL,
  `blood_group` varchar(111) NOT NULL,
  `amount` varchar(111) NOT NULL,
  `address` varchar(111) NOT NULL,
  `date_of_donation` varchar(111) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `request_blood`
--

INSERT INTO `request_blood` (`id`, `name`, `contact`, `blood_group`, `amount`, `address`, `date_of_donation`) VALUES
(1, 'Mazharul Islam', '01868135578', 'O Positive(+)', '32', '234', 'dfsadf'),
(2, 'Mazharul Islam', '01868135578', 'A1B Positive(+)', '3', 'DHAKA.,SDKFJSDKLFJSLDJFLSDJFL', '2019-09-10'),
(3, 'Mazharul Islam', '', 'A1B Positive(+)', '', 'DHAKA.,SDKFJSDKLFJSLDJFLSDJFL', '2019-09-10');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `blood_bank`
--
ALTER TABLE `blood_bank`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `donars`
--
ALTER TABLE `donars`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `message`
--
ALTER TABLE `message`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `request_blood`
--
ALTER TABLE `request_blood`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `blood_bank`
--
ALTER TABLE `blood_bank`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `donars`
--
ALTER TABLE `donars`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `message`
--
ALTER TABLE `message`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `request_blood`
--
ALTER TABLE `request_blood`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
