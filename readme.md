

**_`project name: Blood Record system`_**

**_`technology use`_**

1.php

2.mysql


**_`how to install project`_**
   
<article class="markdown-body entry-content" itemprop="text">
  **************************************************************<ul>
  <li> Clone the repository with git clone </li>
  <li> composer update</li>
  <li> database import</li>
   </ul>**************************************************************

</article>

**_`project home page url`_**:https://blood-record-system.dev

**_`project admin url`_**:https://blood-record-system.dev/view/admin/