<?php
/**
 * Created by PhpStorm.
 * User: Mazed
 * Date: 9/8/2019
 * Time: 12:47 PM
 */

namespace App\BloodBank;
use App\Model\Database;
use PDO;
class BloodBank extends Database
{

    public $id;
    public $name;
    public $category;
    public $location;
    public $contact;

    public function setData($data)
    {
        if(array_key_exists('id',$data))
        {
            $this->id=$data['id'];
        }

        if(array_key_exists('name',$data))
        {
            $this->name=$data['name'];
        }
        if(array_key_exists('category',$data))
        {
            $this->category=$data['category'];
        }

        if(array_key_exists('location',$data))
        {
            $this->location=$data['location'];
        }
        if(array_key_exists('contact',$data))
        {
            $this->contact=$data['contact'];
        }

        return $this;

    }

    public function bloodStore()
    {
        $bloodData=array($this->name,$this->category,$this->location,$this->contact);
        $sql="INSERT INTO `blood_bank` (`name`,`category`,`location`,`contact`) VALUES (?,?,?,?);";
        $prepare=$this->DBH->prepare($sql);
        return $prepare->execute($bloodData);

    }
    public function index()
    {
        $sql="SELECT * FROM `blood_bank`";
        $sth=$this->DBH->query($sql);
        $sth->setFetchMode(PDO::FETCH_OBJ);
        $alldata=$sth->fetchAll();
        return $alldata;
    }
    public function edit()
    {
        $sql="SELECT * FROM `blood_bank` WHERE id = '$this->id'";
        $sth=$this->DBH->query($sql);
        $sth->setFetchMode(PDO::FETCH_OBJ);
        $editdata=$sth->fetch();
        return $editdata;
    }

    public function update()
    {
        $updateData=array($this->name,$this->category,$this->location,$this->contact);
        $sql="UPDATE `blood_bank` SET `name` = ?, `category` = ?, `location` = ?, `contact`=? WHERE `blood_bank`.`id` = '$this->id';";
        $prepare=$this->DBH->prepare($sql);
        $data=$prepare->execute($updateData);
        return $data;
    }


    public function delete()
    {
        $sql="DELETE FROM `blood_bank` WHERE `blood_bank`.`id` ='$this->id'";
        $delete=$this->DBH->exec($sql);
        return $delete;
    }

}