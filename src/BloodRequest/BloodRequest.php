<?php
/**
 * Created by PhpStorm.
 * User: Mazed
 * Date: 9/10/2019
 * Time: 8:23 PM
 */

namespace App\BloodRequest;
use App\Model\Database;
use App\Utility\Utility;
use PDO;
use App\Message\Message;

class BloodRequest extends Database
{
    public $name;
    public $contact;
    public $blood_group;
    public $amount;
    public $address;
    public $date_of_donation;

    public function setData($data)
    {
        if(array_key_exists('name',$data))
        {
            $this->name=$data['name'];
        }
        if(array_key_exists('contact',$data))
        {
            $this->contact=$data['contact'];
        }

        if(array_key_exists('blood_group',$data))
        {
            $this->blood_group=$data['blood_group'];
        }
        if(array_key_exists('amount',$data))
        {
            $this->amount=$data['amount'];
        }
        if(array_key_exists('date_of_donation',$data))
        {
            $this->date_of_donation=$data['date_of_donation'];
        }
        if(array_key_exists('address',$data))
        {
            $this->address=$data['address'];
        }


        return $this;
    }

    public function store()
    {

        $arrayData=array($this->name,$this->contact,$this->blood_group,$this->amount,$this->address,$this->date_of_donation);

        $sql="INSERT INTO `request_blood` (`name`,`contact`,`blood_group`,`amount`,`address`,`date_of_donation`) VALUES (?,?,?,?,?,?);";

        $prepare=$this->DBH->prepare($sql);

        $succes=$prepare->execute($arrayData);

        if($succes){
            Message::message("<span style='color: green'><strong>Success!</strong>Blood Request has been insert successfully.</span>");

        }
        else {
            Message::message("<span style='color: red'><strong>Error!</strong>Error Insert</span>");

        }

    }
    public function index()
    {
        $sql="SELECT * FROM `request_blood`";
        $sth=$this->DBH->query($sql);
        $sth->setFetchMode(PDO::FETCH_OBJ);
        $alldata=$sth->fetchAll();
        return $alldata;
    }



}