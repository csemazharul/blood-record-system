<?php
/**
 * Created by PhpStorm.
 * User: Mazed
 * Date: 9/8/2019
 * Time: 12:33 PM
 */

namespace App\Donar;
use App\Model\Database;
use App\Utility\Utility;
use PDO;
use App\Message\Message;



class Donation extends Database
{
    public $name;
    public $gender;
    public $age;
    public $blood_group;
    public $weight;
    public $address;
    public $contact;
    public $email;
    public $message;
    public $search;

    public function setData($data)
    {
        if(array_key_exists('name',$data))
        {
            $this->name=$data['name'];
        }
        if(array_key_exists('gender',$data))
        {
            $this->gender=$data['gender'];
        }
        if(array_key_exists('age',$data))
        {
            $this->age=$data['age'];
        }
        if(array_key_exists('blood_group',$data))
        {
            $this->blood_group=$data['blood_group'];
        }
        if(array_key_exists('weight',$data))
        {
            $this->weight=$data['weight'];
        }
        if(array_key_exists('address',$data))
        {
            $this->address=$data['address'];
        }
        if(array_key_exists('contact',$data))
        {
            $this->contact=$data['contact'];
        }
        if(array_key_exists('email',$data))
        {
            $this->email=$data['email'];
        }
        if(array_key_exists('message',$data))
        {
            $this->message=$data['message'];
        }
        if(array_key_exists('search',$data))
        {
            $this->search=$data['search'];
        }
        return $this;
    }

    public function store()
    {
            $arrayData=array($this->name,$this->gender,$this->blood_group,$this->age,$this->weight,$this->address,$this->contact,$this->email);
            $sql="INSERT INTO `blood_record`.`donars` (`name`,`gender`,`blood_group`,`age`,`weight`,`address`,`contact`,`email`) VALUES (?,?,?,?,?,?,?,?);";
            $prepare=$this->DBH->prepare($sql);
            $succes=$prepare->execute($arrayData);

        if($succes){
            Message::message("<span style='color: green'><strong>Success!</strong>Donation has been insert successfully.</span>");

        }
        else {
            Message::message("<span style='color: red'><strong>Error!</strong>Error Insert</span>");

        }


    }
    public function index()
    {
        $sql="SELECT * FROM `donars`";
        $sth=$this->DBH->query($sql);
        $sth->setFetchMode(PDO::FETCH_OBJ);
        $alldata=$sth->fetchAll();
        return $alldata;
    }

    public function message(){

        $arrayData=array($this->name,$this->email,$this->contact,$this->message);
        $sql="INSERT INTO `message` (`name`,`email`,`contact`,`message`) VALUES (?,?,?,?);";
        $prepare=$this->DBH->prepare($sql);
        $succes=$prepare->execute($arrayData);


        if($succes){
            Message::message("<span style='color: green'><strong>Success!</strong> Email has been sent successfully.</span>");

        }
        else {
            Message::message("<span style='color: red'><strong>Error!</strong>Email Message could not be sent. Mailer Error:</span>");

        }


        return $succes;
    }


    public function allMessages()
    {
        $sql="SELECT * FROM `message`";
        $sth=$this->DBH->query($sql);
        $sth->setFetchMode(PDO::FETCH_OBJ);
        $allData=$sth->fetchAll();
        return $allData;
    }


    public function searchBlood()
    {
        $search=$this->search;
//
//        $sql = "";
//        if( isset($requestArray['byTitle']) && isset($requestArray['byAuthor']) )  $sql = "SELECT * FROM `book_title` WHERE `is_trashed` ='NULL' AND (`book_title` LIKE '%".$requestArray['search']."%' OR `author_name` LIKE '%".$requestArray['search']."%')";
//        if(isset($requestArray['byTitle']) && !isset($requestArray['byAuthor']) ) $sql = "SELECT * FROM `book_title` WHERE `is_trashed` ='NULL' AND `book_title` LIKE '%".$requestArray['search']."%'";
//        if(!isset($requestArray['byTitle']) && isset($requestArray['byAuthor']) )  $sql = "SELECT * FROM `book_title` WHERE `is_trashed` ='NULL' AND `author_name` LIKE '%".$requestArray['search']."%'";

        $sql = "SELECT * FROM `donars` WHERE  (`name` LIKE '%".$search."%' OR `blood_group` LIKE '%".$search."%')";

        $sth=$this->DBH->query($sql);
        $sth->setFetchMode(PDO::FETCH_OBJ);
        $allData=$sth->fetchAll();
        return $allData;
    }


}