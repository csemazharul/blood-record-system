<?php
require'../../../vendor/autoload.php';


$data=new \App\BloodRequest\BloodRequest();

$requests=$data->index();

?>
<?php include "../include/header.php"?>

<div class="wrapper">
    <!-- Sidebar Holder -->
    <nav id="sidebar">
        <?php  @include('../include/sidebar.php') ?>
    </nav>

    <!-- Page Content Holder -->
    <div id="content">
        <!-- top-bar -->
        <?php include ('../include/navbar.php')?>

        <div class="card">
            <div class="card-header bg-success">
                <div class="float-left text-white">
                    Blood Request List
                </div>

            </div>
            <div class="card-body">

                <table class="table table-bordered table-striped">
                    <tr>

                        <th>Name</th>
                        <th>Contact</th>
                        <th>Blood Group</th>
                        <th>Amount</th>
                        <th>Address</th>
                        <th>Donation Date</th>


                    </tr>
                    <?php


                    foreach ($requests as $request)
                    {
                        echo "
                         <tr>
                               
                               <td>$request->name</td>
                               <td>$request->contact</td>
                               <td>$request->blood_group</td>
                               <td>$request->amount</td>
                               <td>$request->address</td>
                               <td>$request->date_of_donation</td>
                                        
                          </tr>
   

                         ";
                    }
                    ?>

                </table>

            </div>
        </div>
    </div>
</div>
<?php include "../include/footer.php"?>


