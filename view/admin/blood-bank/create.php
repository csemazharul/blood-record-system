<?php
require'../../../vendor/autoload.php';
use App\BloodBank\BloodBank;
use App\Utility\Utility;


$data=new BloodBank();
$data->setData($_POST);

$msg=$data->bloodStore();

?>
<?php include "../include/header.php"?>

<div class="wrapper">
    <!-- Sidebar Holder -->
    <nav id="sidebar">
        <?php  @include('../include/sidebar.php') ?>
    </nav>

    <!-- Page Content Holder -->
    <div id="content">
        <!-- top-bar -->
        <?php include ('../include/navbar.php')?>
        <div class="card">
            <div class="card-header bg-success">
                <div class="float-left text-white">
                    Create Blood Bank
                </div>
                <div class="float-right">
                    <a href="list.php" class="btn btn-primary">List</a>
                </div>
            </div>
            <div class="card-body">

                <form action="store.php" method="post" >

                    <div class="form-group row">
                        <label for="title" class="col-sm-2 col-form-label">Title</label>

                        <div class="col-sm-10">
                            <input type="text" name="name"  placeholder="" id="" class="form-control">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="title" class="col-sm-2 col-form-label">Category</label>

                        <div class="col-sm-10">
                            <select name="category" class="form-control">
                                <option disabled selected>Choose Category</option>
                                <option value="local">Local</option>
                                <option value="international">International</option>
                                <option value="private">Private</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="title" class="col-sm-2 col-form-label">Location</label>

                        <div class="col-sm-10">
                            <input type="text" name="location" placeholder="" id="" class="form-control">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="title" class="col-sm-2 col-form-label">Contact</label>

                        <div class="col-sm-10">
                            <input type="text" name="contact" placeholder="" id="" class="form-control">
                        </div>
                    </div>


                    <div class="form-group row">
                        <div class="col-sm-10 text-center">
                            <input value="submit" type="submit" class="btn btn-primary">
                        </div>
                    </div>

                </form>

            </div>
        </div>
    </div>
</div>
<?php include "../include/footer.php"?>


