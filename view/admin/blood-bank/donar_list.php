<?php
require'../../../vendor/autoload.php';


$data=new \App\Donar\Donation();

$donators=$data->index();

?>
<?php include "../include/header.php"?>

<div class="wrapper">
    <!-- Sidebar Holder -->
    <nav id="sidebar">
        <?php  @include('../include/sidebar.php') ?>
    </nav>

    <!-- Page Content Holder -->
    <div id="content">
        <!-- top-bar -->
        <?php include ('../include/navbar.php')?>

        <div class="card">
            <div class="card-header bg-success">
                <div class="float-left text-white">
                    Donar List
                </div>

            </div>
            <div class="card-body">

                <table class="table table-bordered table-striped">
                    <tr>

                        <th>Name</th>
                        <th>Gender</th>
                        <th>Blood Group</th>
                        <th>Age</th>
                        <th>Address</th>
                        <th>Contact No</th>
                        <th>Email</th>

                    </tr>
                    <?php


                    foreach ($donators as $donator)
                    {
                        echo "
                         <tr>
                               
                               <td>$donator->name</td>
                               <td>$donator->gender</td>
                               <td>$donator->blood_group</td>
                               <td>$donator->age</td>
                               <td>$donator->address</td>
                               <td>$donator->contact</td>
                               <td>$donator->email</td>
                              
                                          
                          </tr>
   

                         ";
                    }
                    ?>

                </table>

            </div>
        </div>
    </div>
</div>
<?php include "../include/footer.php"?>


