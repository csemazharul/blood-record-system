<?php
require'../../../vendor/autoload.php';
use App\BloodBank\BloodBank;
use App\Utility\Utility;

$data=new BloodBank();
$data->setData($_GET);
$editData=$data->edit();

?>

<?php include "../include/header.php"?>

<div class="wrapper">
    <!-- Sidebar Holder -->
    <nav id="sidebar">
        <?php  @include('../include/sidebar.php') ?>
    </nav>

    <!-- Page Content Holder -->
    <div id="content">
        <!-- top-bar -->
        <nav class="navbar navbar-default mb-xl-5 mb-4">
            <div class="container-fluid">

                <div class="navbar-header">
                    <button type="button" id="sidebarCollapse" class="btn btn-info navbar-btn">
                        <i class="fas fa-bars"></i>
                    </button>
                </div>
                <!-- Search-from -->
                <form action="#" method="post" class="form-inline mx-auto search-form">
                    <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search" required="">
                    <button class="btn btn-style my-2 my-sm-0" type="submit">Search</button>
                </form>

                <!--// Search-from -->
                <ul class="top-icons-agileits-w3layouts float-right">

                    <li class="nav-item dropdown mx-3">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown1" role="button" data-toggle="dropdown" aria-haspopup="true"
                           aria-expanded="false">
                            <i class="fas fa-spinner"></i>
                        </a>
                        <div class="dropdown-menu top-grid-scroll drop-2">
                            <h3 class="sub-title-w3-agileits">Shortcuts</h3>
                            <a href="" class="dropdown-item mt-3">
                                <h4>
                                    <i class="fab fa-connectdevelop mr-3"></i>Directories</h4>
                            </a>
                            <a href="" class="dropdown-item mt-3">
                                <h4>
                                    <i class="fas fa-chart-pie mr-3"></i>Files</h4>
                            </a>

                        </div>
                    </li>
                </ul>

                <!--// Copyright -->
            </div>
        </nav>
        <div class="card">
            <div class="card-header bg-success">
                <div class="float-left text-white">
                    Create Blood Bank
                </div>
                <div class="float-right">
                    <a href="list.php" class="btn btn-primary">List</a>
                </div>
            </div>
            <div class="card-body">

                <form action="update.php" method="post" >
                    <input type="hidden" value="<?php echo $editData->id ?>" name="id">
                    <div class="form-group row">
                        <label for="title" class="col-sm-2 col-form-label">Title</label>

                        <div class="col-sm-10">
                            <input type="text" value="<?php echo $editData->name ?>" name="name"  placeholder="" id="" class="form-control">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="title" class="col-sm-2 col-form-label">Category</label>

                        <div class="col-sm-10">
                            <select name="category" class="form-control">
                                <option value="local" <?php if($editData->category=='local') {echo 'selected';} ?> >Local</option>
                                <option value="international" <?php if($editData->category=='international') {echo 'selected';} ?>>International</option>
                                <option value="private" <?php if($editData->category=='private') {echo 'selected';} ?>>Private</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="title" class="col-sm-2 col-form-label">Location</label>

                        <div class="col-sm-10">
                            <input type="text" value="<?php echo $editData->location ?>" name="location" placeholder="" id="" class="form-control">
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="title" class="col-sm-2 col-form-label">Contact</label>

                        <div class="col-sm-10">
                            <input type="text" value="<?php echo $editData->contact ?>" name="contact" placeholder="" id="" class="form-control">
                        </div>
                    </div>


                    <div class="form-group row">
                        <div class="col-sm-10 text-center">
                            <input value="update" type="submit" class="btn btn-primary">
                        </div>
                    </div>

                </form>

            </div>
        </div>
    </div>
</div>
<?php include "../include/footer.php"?>



