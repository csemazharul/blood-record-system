<?php
require'../../../vendor/autoload.php';
use App\BloodBank\BloodBank;
use App\Utility\Utility;

$data=new BloodBank();

$allData=$data->index();

?>
<?php include "../include/header.php"?>

<div class="wrapper">
    <!-- Sidebar Holder -->
    <nav id="sidebar">
        <?php  @include('../include/sidebar.php') ?>
    </nav>

    <!-- Page Content Holder -->
    <div id="content">
        <!-- top-bar -->
        <?php include ('../include/navbar.php')?>



        <div class="card">
            <div class="card-header bg-success">
                <div class="float-left text-white">
                    Blood Bank
                </div>
                <div class="float-right">

                    <a href="create.php" class="btn btn-primary">Add New</a>
                </div>
            </div>
            <div class="card-body">

                <table class="table table-bordered table-striped">
                    <tr>

                        <th>Name</th>
                        <th>Category</th>
                        <th>Location</th>
                        <th>Contact No</th>
                        <th width="200" class="text-right">Action</th>
                    </tr>
                    <?php


                    foreach ($allData as $data)
                    {
                        echo "
                         <tr>
                               
                               <td>$data->name</td>
                               <td>$data->category</td>
                               <td>$data->location</td>
                               <td>$data->contact</td>
                                <td class='text-right'>
                                 
                                  <a href='edit.php?id=$data->id' class='btn btn-sm btn-warning'>Edit</a>
                                  <a href='delete.php?id=$data->id'  class='btn btn-sm btn-danger'>Delete</a>
                                </td>
                                          
                          </tr>
   

                         ";
                    }
                    ?>

                </table>

            </div>
        </div>
    </div>
</div>
<?php include "../include/footer.php"?>


