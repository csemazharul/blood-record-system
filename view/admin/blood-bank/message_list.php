<?php
require'../../../vendor/autoload.php';


$data=new \App\Donar\Donation();

$messages=$data->allMessages();


?>
<?php include "../include/header.php"?>

<div class="wrapper">
    <!-- Sidebar Holder -->
    <nav id="sidebar">
        <?php  @include('../include/sidebar.php') ?>
    </nav>

    <!-- Page Content Holder -->
    <div id="content">
        <!-- top-bar -->
        <?php include ('../include/navbar.php')?>

        <div class="card">
            <div class="card-header bg-success">
                <div class="float-left text-white">
                    Message
                </div>

            </div>
            <div class="card-body">

                <table class="table table-bordered table-striped">
                    <tr>

                        <th>Name</th>
                        <th>Email</th>
                        <th>Contact</th>
                        <th>Message</th>
                    </tr>
                    <?php


                    foreach($messages as $message)
                    {
                        echo "
                         <tr>
                               
                               <td>$message->name</td>
                               <td>$message->email</td>
                               <td>$message->contact</td>
                               <td>$message->message</td>
          
                                        
                          </tr>
   

                         ";
                    }
                    ?>

                </table>

            </div>
        </div>
    </div>
</div>
<?php include "../include/footer.php"?>


