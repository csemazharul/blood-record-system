
<nav class="navbar navbar-default mb-xl-5 mb-4">
    <div class="container-fluid">

        <div class="navbar-header">
            <button type="button" id="sidebarCollapse" class="btn btn-info navbar-btn">
                <i class="fas fa-bars"></i>
            </button>
        </div>


        <!--// Search-from -->
        <ul class="top-icons-agileits-w3layouts float-right">

            <li class="nav-item dropdown mx-3">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown1" role="button" data-toggle="dropdown" aria-haspopup="true"
                   aria-expanded="false">
                    <i class="fas fa-spinner"></i>
                </a>
                <div class="dropdown-menu top-grid-scroll drop-2">
                    <h3 class="sub-title-w3-agileits">Shortcuts</h3>
                    <a href="../../../view/admin/blood-bank/list.php" class="dropdown-item mt-3">
                        <h4>
                            <i class=""></i>Blood Bank</h4>
                    </a>
                    <a href="../../../view/admin/blood-bank/donar_list.php" class="dropdown-item mt-3">
                        <h4>
                            <i class=""></i>Donars</h4>
                    </a>

                    <a href="../../../view/admin/blood-bank/blood_request.php" class="dropdown-item mt-3">
                        <h4>
                            <i class=""></i>Blood Request</h4>
                    </a>


                    <a href="../../../view/admin/blood-bank/message_list.php" class="dropdown-item mt-3">
                        <h4>
                            <i class=""></i>Message</h4>
                    </a>

                </div>
            </li>
        </ul>

        <!--// Copyright -->
    </div>
</nav>