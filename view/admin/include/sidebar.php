<div class="sidebar-header">
    <h1>
        <a href="../../../view/admin/index.php">Home</a>
    </h1>
    <span>E</span>
</div>
<div class="profile-bg"></div>
<ul class="list-unstyled components">
    <li class="active">
        <a href="../../../view/admin/index.php">
            <i class="fas fa-th-large"></i>
            Dashboard
        </a>
    </li>

    <li>
        <a href="#homeSubmenu" data-toggle="collapse" aria-expanded="false">
            <i class="fa fa-shopping-bag"></i>
            Blood Bank
            <i class="fas fa-angle-down fa-pull-right"></i>
        </a>
        <ul class="collapse list-unstyled" id="homeSubmenu">
            <li>
                <a href="../../../view/admin/blood-bank/create.php">Add</a>
            </li>
            <li>
                <a href="../../../view/admin/blood-bank/list.php">List</a>
            </li>
        </ul>
    </li>

    <li>
        <a href="../../../view/admin/blood-bank/donar_list.php">
            <i class="fas fa-users"></i>
            Donator

        </a>

    </li>

    <li>
        <a href="../../../view/admin/blood-bank/blood_request.php">
            <i class="fas fa-arrow-circle-down"></i>
            Blood Request

        </a>
            </li>

    <li>
        <a href="../../../view/admin/blood-bank/message_list.php">
            <i class="fa fa-comment-alt"></i>
            Messages

        </a>

    </li>

</ul>
