
<?php include "include/header.php"?>

    <div class="wrapper">
        <!-- Sidebar Holder -->
        <nav id="sidebar">
            <?php  @include('include/sidebar.php') ?>
        </nav>

        <!-- Page Content Holder -->
        <div id="content">
            <!-- top-bar -->
            <?php include ('include/navbar.php')?>

            <div class="container-fluid">
                <div class="row">

                    <div class="outer-w3-agile col-xl">
                        <div class="stat-grid p-3 d-flex align-items-center justify-content-between bg-primary">
                            <div class="s-l">
                                <h5>Blood Banks</h5>
                            </div>
                            <div class="s-r">
                                <h6>
                                    <i class="fa fa-shopping-bag"></i>
                                </h6>
                            </div>
                        </div>
                        <div class="stat-grid p-3 mt-3 d-flex align-items-center justify-content-between bg-success">
                            <div class="s-l">
                                <h5>Donator</h5>
                            </div>
                            <div class="s-r">
                                <h6>
                                    <i class="fa fa-users"></i>
                                </h6>
                            </div>
                        </div>

                        <div class="stat-grid p-3 mt-3 d-flex align-items-center justify-content-between bg-warning">
                            <div class="s-l">
                                <h5>Blood Request</h5>
                            </div>
                            <div class="s-r">
                                <h6>
                                    <i class="fas fa-arrow-circle-down"></i>
                                </h6>
                            </div>
                        </div>

                        <div class="stat-grid p-3 mt-3 d-flex align-items-center justify-content-between bg-warning">
                            <div class="s-l">
                                <h5>Message</h5>
                            </div>
                            <div class="s-r">
                                <h6>
                                    <i class="fa fa-comment-alt"></i>
                                </h6>
                            </div>
                        </div>

                    </div>

                </div>
            </div>


        </div>
    </div>
<?php include "include/footer.php"?>