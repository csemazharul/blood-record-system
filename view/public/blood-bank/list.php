<?php
require'../../../vendor/autoload.php';
use App\BloodBank\BloodBank;
use App\Utility\Utility;

$data=new BloodBank();

$bloodData=$data->index();

?>
<?php include('../include/header.php')?>
<section id="maincontent">
    <div class="container-fluid">
        <div class="row">
            <div class="middlecontent col-md-8 offset-md-2">
                <h3 class="text-center"><a href="list.php">Blood Bank</a></h3>
                <div class="subcontent">
                    <table class="table table-responsive table-bordered table-hover">
                        <thead class="text-center">

                        <th>Complex Name</th>
                        <th>Category</th>
                        <th>Address</th>
                        <th>Number</th>


                        </thead>
                        <tbody class="text-center">
                        <?php
                        foreach ($bloodData as $data)
                        echo "
                            <tr class='text-center'>
                            <td>$data->name</td>
                            <td>$data->category</td>
                            <td>$data->location</td>
                            <td>$data->contact</td>
                            
                            
                            </tr>
                        ";
                        ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>
<?php include('../include/footer.php')?>
