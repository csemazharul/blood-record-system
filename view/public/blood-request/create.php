<?php
//require'../../../vendor/autoload.php';
//use App\BloodBank\BloodBank;
//use App\Utility\Utility;
//
//$data=new BloodBank();
//
//$bloodData=$data->index();
//
//?>
<?php include('../include/header.php')?>
<section id="maincontent">
    <div class="container-fluid">
        <div class="row">
            <div class="middlecontent col-md-6 offset-md-3">
                <h3 class="text-center"><a href="bloodbank.php">Blood Request&nbsp;<i class="fas fa-eye"></i></a></h3>
                <div class="subcontent">
                    <div class="middlecontent col-md-12">
                        <form action="store.php" class="row" method="post">

                            <label id="name">Name</label>
                            <input type="text" id="name" class="form-control" placeholder="Enter Your Name" name="name">

                            <label id="name">Your Contact Number</label>
                            <input type="text" id="name" class="form-control" placeholder="Enter Your Number" name="contact">

                            <label id="blood_group">Blood Group</label>
                            <select id="blood_group" name="blood_group" class="form-control">
                                <option disabled selected>Choose Blood Group</option>
                                <option value="A Positive(+)">A Positive(+)</option>
                                <option value="A Negative(-)">A Negative(-)</option>
                                <option value="B Positive(+)">B Positive(+)</option>
                                <option value="B Negative(-)">B Negative(-)</option>
                                <option value="O Positive(+)">O Positive(+)</option>
                                <option value="O Negative(-)">O Negative(-)</option>
                                <option value="AB Positive(+)">AB Positive(+)</option>
                                <option value="AB Negative(-)">AB Negative(-)</option>
                                <option value="A1 Positive(+)">A1 Positive(+)</option>
                                <option value="A1 Negative(-)">A1 Negative(-)</option>
                                <option value="A1B Positive(+)">A1B Positive(+)</option>
                                <option value="A1B Negativ(-)">A1B Negativ(-)</option>
                                <option value="A2 Positive(+)">A2 Positive(+)</option>
                                <option value="A2 Negative(-)">A2 Negative(-)</option>
                                <option value="A2B Positive(+)">A2B Positive(+)</option>
                            </select>

                            <label id="age">Blood Amount (unit/bag)</label>
                            <input type="number" id="amount" class="form-control" placeholder="Enter Your Amount (unit/bag)" name="amount">

                            <label id="address">Present Location</label>
                            <input type="text"  id="district" class="form-control" placeholder="Enter Your Present Location" name="address">


                            <label id="donation_date">Date Of Donation</label>
                            <input type="date" class="form-control" name="date_of_donation" id="donation_of_date">
                            <input type="submit" value="SUBMIT" class="form-control btn btn-primary mt-2 m-4">
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php include('../include/footer.php')?>
