<?php
require'../../../vendor/autoload.php';
use App\BloodRequest\BloodRequest;
use App\Utility\Utility;
use App\Message\Message;
$data=new BloodRequest();
$msg=Message::message();

$bloodRequest=$data->index();

?>
<?php include('../include/header.php')?>
<section id="maincontent">
    <div class="container-fluid">
        <div class="row">
            <div class="middlecontent col-md-8 offset-2">
                <h3 class="text-center"><a href="list.php">Blood Request List</a></h3>
                <div class="row">
                    <?php  if(isset($msg)){echo "<div id='message' style=\"padding: 8px;text-align: center; font-size: 20px;\">". $msg ."</div>";}?>


                <div class="subcontent" id="email_cache">
                    <table class="table table-responsive table-bordered table-hover">
                        <thead class="text-center">

                        <th>Name</th>
                        <th>Contact Number</th>
                        <th>Blood Group</th>
                        <th>Amount Blood</th>
                        <th>Address</th>
                        <th>Donation Date</th>


                        </thead>
                        <tbody class="text-center">
                        <?php
                        foreach ($bloodRequest as $data)
                            echo "
                            <tr class='text-center'>
                            <td class='name'>$data->name</td>
                            <td>$data->contact</td>
                            <td>$data->blood_group</td>
                            <td>$data->amount</td>
                            <td>$data->address</td>
                            <td>$data->date_of_donation</td>
                         
                            </tr>
                        ";
                        ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

    <script>


        jQuery(

            function($) {
                $('#message').fadeOut (850);
                $('#message').fadeIn (850);
                $('#message').fadeOut (850);
                $('#message').fadeIn (850);
                $('#message').fadeOut (850);
            }
        )
    </script>

<?php include('../include/footer.php')?>