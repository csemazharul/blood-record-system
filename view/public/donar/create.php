<?php
//require'../../../vendor/autoload.php';
//use App\BloodBank\BloodBank;
//use App\Utility\Utility;
//
//$data=new BloodBank();
//
//$bloodData=$data->index();
//
//?>
<?php include('../include/header.php')?>
<section id="maincontent">
    <div class="container-fluid">
        <div class="row">
            <div class="middlecontent col-md-6 offset-md-3">
                <h3 class="text-center"><a >Donar Registration</a></h3>
                <div class="subcontent">
                    <div class="middlecontent col-md-12">
                        <form action="store.php" class="row" method="post">

                                <label id="name">Name</label>
                                <input type="text" id="name" class="form-control" placeholder="Enter Your Name" name="name">

                            <label id="gender">Gender</label>
                            <select name="gender" class="form-control" id="gender">
                                <option disabled selected>Choose Gender</option>
                                <option value="male">Male</option>
                                <option value="female">Femaale</option>
                            </select>

                            <label id="blood_group">Blood Group</label>
                            <select id="blood_group" name="blood_group" class="form-control">
                                <option disabled selected>Choose Blood Group</option>
                                <option value="A Positive(+)">A Positive(+)</option>
                                <option value="A Negative(-)">A Negative(-)</option>
                                <option value="B Positive(+)">B Positive(+)</option>
                                <option value="B Negative(-)">B Negative(-)</option>
                                <option value="O Positive(+)">O Positive(+)</option>
                                <option value="O Negative(-)">O Negative(-)</option>
                                <option value="AB Positive(+)">AB Positive(+)</option>
                                <option value="AB Negative(-)">AB Negative(-)</option>
                                <option value="A1 Positive(+)">A1 Positive(+)</option>
                                <option value="A1 Negative(-)">A1 Negative(-)</option>
                                <option value="A1B Positive(+)">A1B Positive(+)</option>
                                <option value="A1B Negativ(-)">A1B Negativ(-)</option>
                                <option value="A2 Positive(+)">A2 Positive(+)</option>
                                <option value="A2 Negative(-)">A2 Negative(-)</option>
                                <option value="A2B Positive(+)">A2B Positive(+)</option>
                            </select>

                            <label id="age">Age</label>
                            <input type="number" id="name" class="form-control" placeholder="Enter Your Age" name="age">

                            <label id="age">Weight</label>
                            <input type="number" id="name" class="form-control" placeholder="Enter Your Age" name="weight">

                            <label id="district">Address</label>
                            <input type="text" id="district" class="form-control" placeholder="Enter Your Name" name="address">


                            <label id="contact_number">Contact Number</label>
                            <input type="number" class="form-control" name="contact" id="contact_number" placeholder="Your Contact Number">
                            <label id="email">Email</label>
                            <input type="email" class="form-control" name="email" id="email" placeholder="Email">

                            <input type="submit" value="SUBMIT" class="form-control btn btn-primary mt-2 m-4">
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php include('../include/footer.php')?>
