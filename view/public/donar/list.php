<?php
require'../../../vendor/autoload.php';
use App\BloodBank\BloodBank;
use App\Utility\Utility;
use App\Message\Message;
$data=new \App\Donar\Donation();
$data->setData($_GET);

$msg=Message::message();

if(isset($_GET['search']))
{
    $donationData=$data->searchBlood();
}
else{
    $donationData=$data->index();
}



?>
<?php include('../include/header.php')?>
<section id="maincontent">
    <div class="container-fluid">
        <div class="row">
            <div class="middlecontent col-md-10 offset-1 ">
                <h3 class="text-center"><a href="list.php">Find Your Blood&nbsp</a></h3>
                <form action="list.php" class="row" method="get">
                    <div class="col-md-2 offset-md-5">
                        <label style="font-size: 26px;color:grey;" class=" font-weight-bold">Search : </label>
                    </div>
                    <div class="col-md-5">
                        <input type="text" class="form-control mb-2 d-inline" name="search" placeholder="Search Blood">
                    </div>
                </form>

                <div class="row">
                    <?php  if(isset($msg)){echo "<div id='message' style=\"padding: 8px;text-align: center; font-size: 20px;\">". $msg ."</div>";}?>

                <div class="subcontent" id="email_cache">
                    <table class="table table-responsive table-bordered table-hover">
                        <thead class="text-center">

                        <th>Donar Name</th>
                        <th>Gender</th>
                        <th>Blood Group</th>
                        <th>Age</th>
                        <th>Address</th>
                        <th>Contact</th>
                        <th>Email</th>
                        <th>Message</th>

                        </thead>
                        <tbody class="text-center">
                        <?php
                        foreach ($donationData as $data)
                            echo "
                            <tr class='text-center'>
                            <td class='name' title='$data->name'>$data->name</td>
                            <td>$data->gender</td>
                            <td>$data->blood_group</td>
                            <td>$data->age</td>
                            <td>$data->age</td>
                            <td>$data->address</td>
                            <td>$data->email</td>
                            <td><input  type='button' src='$data->email' value='Email' name='view' class='view_data btn btn-secondary'>
                            
                            </tr>
                        ";
                        ?>
                        </tbody>
                    </table>
                </div>

                <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered" role="document">
                        <div class="modal-content">
                            <div class="modal-header">

                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <form name="myform" action="message.php" method="post">
                                    <label>Name</label>
                                    <input type="text" id="name"  name="name" class="form-control">


                                    <label>contact</label>
                                    <input type="text" name="contact" class="form-control">

                                    <input type="hidden" name="email" id="email" value="" class="form-control">

                                    <label>Message</label>
                                    <textarea class="form-control" rows="5" cols="5" name="message"></textarea>

                                </form>
                            </div>
                            <div class="modal-footer">
                                <button type="button"  class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                                <button type="button" id="submitForm" class="btn btn-primary">Submit</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<script>

    jQuery(

        function($) {
            $('#message').fadeOut (850);
            $('#message').fadeIn (850);
            $('#message').fadeOut (850);
            $('#message').fadeIn (850);
            $('#message').fadeOut (850);
        }
    );


    $(document).ready(function () {


        $(".view_data").click(function () {
//            let email=$(this).attr('id');

            var donarId=$(this).attr('src');
            $("#exampleModalCenter").modal("show");
            $("#email").val(donarId);
            var title="<h5 class='modal-title' id='exampleModalLongTitle'>Send E-mail to "+donarId+"</h5>";
            $(".modal-header").html(title);


        });
    });

    $(document).ready(function () {
    $("#submitForm").click(function () {
        document.myform.submit();
    });
    });


</script>

<?php include('../include/footer.php')?>
