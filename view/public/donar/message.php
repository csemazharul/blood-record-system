<?php
require '../../../vendor/autoload.php';

use App\Donar\Donation;
use App\Utility\Utility;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

$yourGmailAddress = 'postmaster@test.myscoresme.com';
$yourGmailPassword = '20c4b1191b6822c9ac9655d9d0bb1905-41a2adb4-7f832e5d';

$data=new Donation();
$data->setData($_POST);
$succes=$data->message();

// Load Composer's autoloader

// Instantiation and passing `true` enables exceptions
$mail = new PHPMailer(true);

try {
//Server settings
$mail->SMTPDebug = 2;                                       // Enable verbose debug output
$mail->isSMTP();                                            // Set mailer to use SMTP
$mail->Host       = 'smtp.mailgun.org';  // Specify main and backup SMTP servers
$mail->SMTPAuth   = true;                                   // Enable SMTP authentication
$mail->Username   = $yourGmailAddress;                    // SMTP username
$mail->Password   = $yourGmailPassword;                               // SMTP password
$mail->SMTPSecure = 'tls';                                  // Enable TLS encryption, `ssl` also accepted
$mail->Port       = 587;                                    // TCP port to connect to

//Recipients
$mail->setFrom($yourGmailAddress, 'Blood Camp');
$mail->addAddress($_POST['email'], $_POST['name']);     // Add a recipient
$mail->addReplyTo($yourGmailAddress, 'Blood Camp');
//   $mail->addCC('cc@example.com');
//   $mail->addBCC('bcc@example.com');

// Content
$mail->isHTML(true);                                  // Set email format to HTML
$mail->Subject = 'Blood Request';
$mail->Body    = "<p>".$_POST['message']."</p>";
$mail->AltBody = $_POST['message'];

$mailSend=$mail->send();
if($mailSend)
{
    Utility::redirect('list.php');
}

} catch (Exception $e) {
echo "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
}