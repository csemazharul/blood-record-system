<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="author" content="">

    <title>Blood Record System</title>
    <!--  CSS -->
    <link rel="stylesheet" href="../../../resource/ui/frontend/css/fontawesome-all.min.css">
    <link rel="stylesheet" href="../../../resource/ui/frontend/css/style.css">
    <script type="text/javascript" src="../../../resource/ui/frontend/js/ajax.js"></script>

    <style>
    .slider_content{

        background: url("resource/ui/backend/images/home_1_slider_1.jpg") no-repeat 0px 0px;

        min-height: 600px;
        /*width:100%;*/
    }
    </style>

</head>
<body>


<!--=============Header Starts============-->



<header id="header" class="fixed-top">
    <div class="container">
        <!--navbar start-->
        <nav class="navbar navbar-expand-lg">
            <!--Your Logo-->
            <a class="nav-link" href="/index.php">Blood Record System</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#myNavbar"
                    aria-controls="myNavbar" aria-expanded="false" aria-label="Toggle navigation">
                <span><i class="fas fa-align-justify" style="color:white"></i></span>
            </button>
            <div class="collapse navbar-collapse" id="myNavbar">
                <ul class="navbar-nav ml-auto">

                    <li class="nav-item">
                        <a class="nav-link" href="../../../view/public/donar/create.php">Donar Registration</a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link" href="../../../view/public/donar/list.php">Donars List</a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link" href="../../../view/public/blood-bank/list.php">Blood Bank</a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link" href="../../../view/public/blood-request/create.php">REQUEST BLOOD</a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link" href="../../../view/public/blood-request/list.php">All REQUEST LIST</a>
                    </li>

                </ul>
            </div>
        </nav>
    </div>
</header>


<!--=============Header End===============-->


